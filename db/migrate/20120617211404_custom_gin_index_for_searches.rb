class CustomGinIndexForSearches < ActiveRecord::Migration
  def up
    execute "CREATE INDEX tsv_GIN ON webs USING GIN(tsv)"
  end

  def down
    execute "DROP INDEX tsv_GIN"
  end
end
