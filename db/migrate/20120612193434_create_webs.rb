class CreateWebs < ActiveRecord::Migration
  def change
    create_table :webs do |t|
      t.integer :user_id
      t.string :title
      t.text :content
      t.string :url

      t.timestamps
    end
  end
end
