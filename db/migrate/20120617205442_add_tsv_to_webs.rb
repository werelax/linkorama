class AddTsvToWebs < ActiveRecord::Migration
  def up
    add_column :webs, :tsv, :TSVECTOR
    execute <<-SQL
      UPDATE webs SET tsv=to_tsvector(COALESCE(title, '') || ' ' || COALESCE(content, '') || ' ' || COALESCE(url, ''))
    SQL
  end

  def down
    remove_column :webs, :tsv
  end
end
