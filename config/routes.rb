Linkrbot::Application.routes.draw do
  unauthenticated do
    root :to => "pages#index"
  end

  authenticated :user do
    root :to => "users#index"
    get 'search' => "webs#index", :as => :search
  end

  devise_for :users
  post 'save' => "webs#create", :as => :webs
end
