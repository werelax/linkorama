require 'base64'

class WebsController < ApplicationController
  before_filter :authenticate_user!, :except => [:create]
  protect_from_forgery :except => :create

  def index
    start = Time.now
    @query = params[:q]
    @results = Web.search(current_user, @query)
    finish = Time.now
    @time = (finish - start) * 1000.0
  rescue
    @query = params[:q]
    @results = []
    @time = 0
  end

  def create
    if user_signed_in?
      params[:content] = Base64.decode64(params[:content])
      params[:title] = Base64.decode64(params[:title])
      web = current_user.webs.build(params.slice(:url, :title, :content))
      @status = web.save ? 'ok' : 'error'
    else
      @status = 'notlogged'
    end
    render :layout => false
  end

  def delete
  end
end
