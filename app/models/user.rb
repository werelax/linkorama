class User < ActiveRecord::Base
  has_many :webs

  devise :database_authenticatable, :registerable, :rememberable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me

end
