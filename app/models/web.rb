class Web < ActiveRecord::Base
  belongs_to :user

  attr_accessible :title, :content, :url

  validates_uniqueness_of :url, :scope => [:user_id]

  after_save :update_tsv

  def self.search(user, query)
    self.find_by_sql([
      "SELECT
         title,
         url,
         content,
         user_id,
         created_at,
         tsv,
         ts_rank(tsv, query) as rank
       FROM
         webs,
         to_tsquery(?) query
       WHERE
         webs.user_id = ? AND
         tsv @@ query
       ORDER BY rank DESC",
      query.split.join(' & '),
      user.id
    ]);
  end

  def update_tsv
    ActiveRecord::Base.connection.execute(
      "UPDATE webs
       SET
         tsv=to_tsvector(COALESCE(title, '') || ' ' || COALESCE(content, '') || ' ' || COALESCE(url, ''))
       WHERE
        webs.id = #{self.id}"
    )
  end

end
