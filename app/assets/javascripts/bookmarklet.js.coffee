#= require base64

# Constants

# Config is defined in the bookmarklet link

Urls =
  save: Config.url '/save'

# Data

images =
  saved: Config.url '/assets/saved.jpg'
  loading: Config.url '/assets/loading.jpg'
  error: Config.url '/assets/error.jpg'
  notlogged: Config.url '/assets/notlogged.jpg'

# Utils

class AjaxService
  @encode_array: (key, array) ->
    lines = []
    keyname = encodeURIComponent(key + '[]')
    for item in array
      lines.push keyname + '=' + encodeURIComponent(item)
    lines.join('&').replace(/%20/g, '+')

  @encode_params: (data) ->
    result = []
    for key, value of data
      continue unless data.hasOwnProperty(key) and !!value
      if (value instanceof Array)
        result.push @encode_array(key, value)
      else
        result.push "#{encodeURIComponent(key)}=#{encodeURIComponent(value)}"
    result.join('&').replace(/%20/g, '+')

  @encode_params_inurl: (url, data) ->
    params = @encode_params(data)
    if params.length > 0 then "#{url}?#{params}" else url

  @encode_params_inbody: (data) ->
    @encode_params(data)

  @handle_state_change = (req, success, error) ->
    return ->
      if req.readyState == 4
        if 200 <= req.status < 300
          success(req.responseText)
        else
          error(req.status)

  @request: (options) ->
    type     = options.type     || 'GET'
    dataType = options.dataType || 'json'
    success  = options.success  || (->)
    error    = options.error    || (->)
    data     = options.data     || {}
    url      = options.url
    body     = undefined

    success = ((data)-> options.success(JSON.parse(data))) if dataType == 'json'

    url  = @encode_params_inurl(url, data) if type == 'GET'
    body = @encode_params_inbody(data)     if type == 'POST'

    req = new XMLHttpRequest()
    req.open(type, url, true)
    req.onreadystatechange = @handle_state_change(req, success, error)
    req.setRequestHeader("Content-type", "application/x-www-form-urlencoded") if type == 'POST'
    req.send(body)

class PostService
  constructor: (url) ->
    @f = document.createElement('iframe')
    @fname = Math.floor(Math.random()*Math.pow(10, 32)).toString(36)
    @f.setAttribute('name', @fname)
    @f.setAttribute('id', @fname)
    @f.setAttribute('allowTransparency', true)
    @f.setAttribute('style', 'display:none')
    document.body.appendChild(@f)

    @form = document.createElement('form')
    @form.setAttribute('method', 'post')
    @form.setAttribute('action', url)
    @form.setAttribute('target', @fname)

  send: (data, cb) ->
    @cb = cb || ->
    field = null
    for key,value of data
      field = document.createElement('input')
      field.setAttribute('type', 'hidden')
      field.setAttribute('name', key)
      field.setAttribute('value', value)
      @form.appendChild(field)
    document.body.appendChild(@form)
    that = @
    @f.onload = (e) -> that._unload(e)
    setTimeout (-> that.form.submit() ), 50

  _unload: (e) ->
    # document.body.removeChild(@f)
    # document.body.removeChild(@form)
    @send = -> throw new Error("Can't send twice with the same instance")
    @cb()

# JSONP

class JSONPService
  @send: (url, data, cb) ->
    s = document.createElement('script')
    cb ||= ->
    real_cb_name = Math.floor(Math.random()*Math.pow(10, 32)).toString(36)
    window[real_cb_name] = (data) ->
      cb(data)
      document.body.removeChild(s)
      delete window[real_cb_name]
    data.__callback__ = real_cb_name
    url = AjaxService.encode_params_inurl(url, data)
    s.setAttribute('src', url)
    document.body.appendChild(s)

class BannerService
  @show: (name, auto_fade=false) ->
    try document.body.removeChild(@img)
    @img = img = document.createElement('img')
    img.setAttribute('src', images[name])
    css = [
      'position:fixed',
      'top: 20%',
      'left: 50%',
      'margin-left: -55px'
    ].join(';')
    img.setAttribute('style', css)
    document.body.appendChild(img)
    remove_image = ->
      try document.body.removeChild(img)
    if auto_fade then setTimeout remove_image, 1000

class ResponseHandler
  @onmessage: (message) ->
    @[message]?()
  @ok: ->
    BannerService.show('saved', true)
  @error: ->
    BannerService.show('error', true)
  @notlogged: ->
    BannerService.show('notlogged', true)

# Logic

BannerService.show('loading')

message_handler = (e) -> ResponseHandler.onmessage(e.data)
window.addEventListener('message', message_handler, false)

data =
  title: Base64.encode(document.title),
  content: Base64.encode(document.body.textContent || document.body.innerText),
  url: window.location.href

poster = new PostService(Urls.save)
poster.send data

# Self-Destruction

# s = document.getElementById('_linkrbot_bookmarklet_script_')
# document.body.removeChild(s)
